package fr.adamaq01.evilpingouin.expressions;

import org.bukkit.event.Event;
import org.eclipse.jdt.annotation.Nullable;

import ch.njol.skript.ScriptLoader;
import ch.njol.skript.Skript;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.skript.log.ErrorQuality;
import ch.njol.util.Kleenean;
import fr.adamaq01.evilpingouin.customevents.PacketEvent;
import fr.adamaq01.evilpingouin.customevents.PacketReceiveEvent;
import fr.adamaq01.evilpingouin.customevents.PacketSendEvent;
import fr.adamaq01.evilpingouin.utils.EventsChecker;
import fr.adamaq01.evilpingouin.utils.Reflection;

public class ExprPacketValue extends SimpleExpression<Object> {

	private Expression<String> fieldName;

	@Override
	public boolean isSingle() {
		return true;
	}

	@Override
	public Class<? extends Object> getReturnType() {
		return Object.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, ParseResult parseResult) {
		this.fieldName = (Expression<String>) exprs[0];
		if (ScriptLoader.isCurrentEvent(PacketSendEvent.class, PacketReceiveEvent.class))
			return true;
		Skript.error("Cannot use packet's value expression outside of a packet event!", ErrorQuality.SEMANTIC_ERROR);
		return false;
	}

	@Override
	public String toString(@Nullable Event e, boolean debug) {
		return this.getClass().getName();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Nullable
	protected Object[] get(Event e) {
		if (!EventsChecker.isEvents(e, PacketSendEvent.class, PacketReceiveEvent.class))
			return new Object[] { "Unknown" };
		return new Object[] { Reflection.getFieldValue(((PacketEvent) e).getPacket(), fieldName.getSingle(e)) };
	}

}
