package fr.adamaq01.evilpingouin.expressions;

import org.bukkit.event.Event;
import org.eclipse.jdt.annotation.Nullable;

import ch.njol.skript.ScriptLoader;
import ch.njol.skript.Skript;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.skript.log.ErrorQuality;
import ch.njol.util.Kleenean;
import fr.adamaq01.evilpingouin.customevents.PacketEvent;
import fr.adamaq01.evilpingouin.customevents.PacketReceiveEvent;
import fr.adamaq01.evilpingouin.customevents.PacketSendEvent;
import fr.adamaq01.evilpingouin.utils.EventsChecker;

public class ExprPacketName extends SimpleExpression<String> {

	@Override
	public boolean isSingle() {
		return true;
	}

	@Override
	public Class<? extends String> getReturnType() {
		return String.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, ParseResult parseResult) {
		if (ScriptLoader.isCurrentEvent(PacketSendEvent.class, PacketReceiveEvent.class))
			return true;
		Skript.error("Cannot use packet's name expression outside of a packet event!", ErrorQuality.SEMANTIC_ERROR);
		return false;
	}

	@Override
	public String toString(@Nullable Event e, boolean debug) {
		return this.getClass().getName();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Nullable
	protected String[] get(Event e) {
		if (!EventsChecker.isEvents(e, PacketSendEvent.class, PacketReceiveEvent.class))
			return new String[] { "Unknown" };
		return new String[] { ((PacketEvent) e).getPacketName() };
	}

}
