package fr.adamaq01.evilpingouin.expressions;

import org.bukkit.event.Event;
import org.eclipse.jdt.annotation.Nullable;

import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import fr.adamaq01.evilpingouin.EvilPingouinSK;

public class ExprRedisSMembers extends SimpleExpression<String> {

	private Expression<String> key;

	@Override
	public boolean isSingle() {
		return false;
	}

	@Override
	public Class<? extends String> getReturnType() {
		return String.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, ParseResult parseResult) {
		key = (Expression<String>) exprs[0];
		return true;
	}

	@Override
	public String toString(@Nullable Event e, boolean debug) {
		return getClass().getName();
	}

	@Override
	@Nullable
	protected String[] get(Event e) {
		return EvilPingouinSK.getInstance().getJedis().smembers(key.getSingle(e)).toArray(new String[] {});
	}

}
