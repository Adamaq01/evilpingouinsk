package fr.adamaq01.evilpingouin.objects;

import org.bukkit.plugin.java.JavaPlugin;

public abstract class Manager {
	
	private JavaPlugin plugin;
	
	protected Manager(JavaPlugin plugin) {
		this.plugin = plugin;
	}
	
	public abstract void register();
	
	public JavaPlugin getPlugin() {
		return plugin;
	}

}
