package fr.adamaq01.evilpingouin.utils;

import org.bukkit.entity.Player;

import fr.adamaq01.evilpingouin.EvilPingouinSK;
import fr.adamaq01.evilpingouin.customevents.PacketReceiveEvent;
import fr.adamaq01.evilpingouin.customevents.PacketSendEvent;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;

public class ChannelHandler extends ChannelDuplexHandler {

	private Player player;

	public ChannelHandler(Player p) {
		this.player = p;
	}

	@Override
	public void write(ChannelHandlerContext c, Object m, ChannelPromise promise) throws Exception {
		PacketSendEvent e = new PacketSendEvent(player, m, m.getClass().getSimpleName());
		EvilPingouinSK.getInstance().getServer().getPluginManager().callEvent(e);
		if (!e.isCancelled()) {
			super.write(c, m, promise);
		}
	}

	@Override
	public void channelRead(ChannelHandlerContext c, Object m) throws Exception {
		PacketReceiveEvent e = new PacketReceiveEvent(player, m, m.getClass().getSimpleName());
		EvilPingouinSK.getInstance().getServer().getPluginManager().callEvent(e);
		if (!e.isCancelled()) {
			super.channelRead(c, m);
		}
	}

}