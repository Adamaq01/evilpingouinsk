package fr.adamaq01.evilpingouin.utils;

import java.lang.reflect.Field;

import org.bukkit.entity.Player;

import io.netty.channel.Channel;

public class PacketInjector {

	private Field EntityPlayer_playerConnection;
	private Class<?> PlayerConnection;
	private Field PlayerConnection_networkManager;

	private Class<?> NetworkManager;
	private Field channel;
	private Field packetListener;

	public PacketInjector() {
		try {
			EntityPlayer_playerConnection = Reflection.getField(Reflection.getClass("{nms}.EntityPlayer"),
					"playerConnection");

			PlayerConnection = Reflection.getClass("{nms}.PlayerConnection");
			PlayerConnection_networkManager = Reflection.getField(PlayerConnection, "networkManager");

			NetworkManager = Reflection.getClass("{nms}.NetworkManager");
			if (Reflection.getNmsVersion().equals("v1_8_R1")) {
				channel = Reflection.getField(NetworkManager, "i");
				packetListener = Reflection.getField(NetworkManager, "k");
			} else if (Reflection.getNmsVersion().equals("v1_8_R2")) {
				channel = Reflection.getField(NetworkManager, "k");
				packetListener = Reflection.getField(NetworkManager, "m");
			} else if (Reflection.getNmsVersion().equals("v1_8_R3")) {
				channel = Reflection.getField(NetworkManager, "channel");
				packetListener = Reflection.getField(NetworkManager, "m");
			} else if (Reflection.getNmsVersion().equals("v1_9_R2")) {
				channel = Reflection.getField(NetworkManager, "channel");
				packetListener = Reflection.getField(NetworkManager, "m");
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	public void addPlayer(Player p) {
		try {
			Channel ch = getChannel(getNetworkManager(Reflection.getNmsPlayer(p)));
			if (ch.pipeline().get("PacketInjector") == null) {
				ch.pipeline().addBefore("packet_handler", "PacketInjector", new ChannelHandler(p));
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	public void removePlayer(Player p) {
		try {
			Channel ch = getChannel(getNetworkManager(Reflection.getNmsPlayer(p)));
			if (ch.pipeline().get("PacketInjector") != null) {
				ch.pipeline().remove("PacketInjector");
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	private Object getNetworkManager(Object ep) {
		Object o = Reflection.getFieldValue(EntityPlayer_playerConnection, ep);
		Object n = Reflection.getFieldValue(PlayerConnection_networkManager, o);
		return n;
	}

	private Channel getChannel(Object networkManager) {
		Channel ch = null;
		try {
			ch = Reflection.getFieldValue(channel, networkManager);
		} catch (Exception e) {
			ch = Reflection.getFieldValue(packetListener, networkManager);
		}
		return ch;
	}
}