package fr.adamaq01.evilpingouin.utils;

import org.bukkit.event.Event;

public class EventsChecker {

	@SuppressWarnings("unchecked")
	public static boolean isEvents(Event e, Class<? extends Event>... events) {
		for (Class<? extends Event> event : events) {
			if (e.getEventName().equals(event.getSimpleName())) {
				return true;
			}
		}
		return false;
	}

}
