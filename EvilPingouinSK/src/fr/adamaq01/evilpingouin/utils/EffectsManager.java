package fr.adamaq01.evilpingouin.utils;

import org.bukkit.plugin.java.JavaPlugin;

import ch.njol.skript.Skript;
import fr.adamaq01.evilpingouin.effects.EffRedisDelete;
import fr.adamaq01.evilpingouin.effects.EffRedisSAdd;
import fr.adamaq01.evilpingouin.effects.EffRedisSRemove;
import fr.adamaq01.evilpingouin.effects.EffRedisSet;
import fr.adamaq01.evilpingouin.objects.Manager;

public class EffectsManager extends Manager {

	public EffectsManager(JavaPlugin plugin) {
		super(plugin);
	}

	@Override
	public void register() {
		Skript.registerEffect(EffRedisSet.class, "redis set %key% to %value%");
		Skript.registerEffect(EffRedisDelete.class, "redis delete %key%");
		Skript.registerEffect(EffRedisSAdd.class, "redis sadd %key% to %value%");
		Skript.registerEffect(EffRedisSRemove.class, "redis sremove %key%");
	}

}
