package fr.adamaq01.evilpingouin.utils;

import org.bukkit.plugin.java.JavaPlugin;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.ExpressionType;
import fr.adamaq01.evilpingouin.expressions.ExprPacketName;
import fr.adamaq01.evilpingouin.expressions.ExprPacketValue;
import fr.adamaq01.evilpingouin.expressions.ExprRedisGet;
import fr.adamaq01.evilpingouin.expressions.ExprRedisSMembers;
import fr.adamaq01.evilpingouin.objects.Manager;

public class ExpressionsManager extends Manager {

	public ExpressionsManager(JavaPlugin plugin) {
		super(plugin);
	}

	@Override
	public void register() {

		// Packets
		Skript.registerExpression(ExprPacketName.class, String.class, ExpressionType.SIMPLE, "packet's name",
				"name of packet");
		Skript.registerExpression(ExprPacketValue.class, Object.class, ExpressionType.SIMPLE, "packet's value %string%",
				"value %string% of packet");

		// Redis
		Skript.registerExpression(ExprRedisGet.class, String.class, ExpressionType.SIMPLE, "redis get %key%");
		Skript.registerExpression(ExprRedisSMembers.class, String.class, ExpressionType.SIMPLE, "redis smembers %key%");
	}

}
