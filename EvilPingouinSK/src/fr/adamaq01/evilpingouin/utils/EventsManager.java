package fr.adamaq01.evilpingouin.utils;

import org.bukkit.plugin.java.JavaPlugin;

import ch.njol.skript.Skript;
import ch.njol.skript.lang.util.SimpleEvent;
import fr.adamaq01.evilpingouin.customevents.PacketReceiveEvent;
import fr.adamaq01.evilpingouin.customevents.PacketSendEvent;
import fr.adamaq01.evilpingouin.objects.Manager;

public class EventsManager extends Manager {

	public EventsManager(JavaPlugin plugin) {
		super(plugin);
	}

	@Override
	public void register() {
		Skript.registerEvent("On Packet Receive", SimpleEvent.class, PacketReceiveEvent.class, "packetreceive", "packet receive");
		Skript.registerEvent("On Packet Send", SimpleEvent.class, PacketSendEvent.class, "packetsend", "packet send");
	}

}
