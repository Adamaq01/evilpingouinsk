package fr.adamaq01.evilpingouin.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class Reflection {

	public static Class<?> getClass(String classname) {
		try {
			String version = getNmsVersion();
			String path = classname.replace("{nms}", "net.minecraft.server." + version)
					.replace("{nm}", "net.minecraft." + version).replace("{cb}", "org.bukkit.craftbukkit." + version);
			return Class.forName(path);
		} catch (Throwable t) {
			t.printStackTrace();
			return null;
		}
	}

	public static String getNmsVersion() {
		return Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
	}

	public static Object getNmsPlayer(Player p) {
		Method getHandle = null;
		try {
			getHandle = p.getClass().getMethod("getHandle");
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		try {
			return getHandle.invoke(p);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return getHandle;
	}

	public static Object getNmsScoreboard(Scoreboard s) {
		Method getHandle = null;
		try {
			getHandle = s.getClass().getMethod("getHandle");
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		try {
			return getHandle.invoke(s);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return getHandle;
	}

	public static Object getFieldValue(Object instance, String fieldName) {
		Field field = null;
		try {
			field = instance.getClass().getDeclaredField(fieldName);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
		field.setAccessible(true);
		try {
			return field.get(instance);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return field;
	}

	public static Object getProtectedFieldValue(Object instance, String fieldName) {
		Field field = null;
		try {
			field = instance.getClass().getSuperclass().getDeclaredField(fieldName);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
		field.setAccessible(true);
		try {
			return field.get(instance);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return field;
	}

	public static Object getNonDeclaredFieldValue(Object instance, String fieldName) {
		Field field = null;
		try {
			field = instance.getClass().getField(fieldName);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
		field.setAccessible(true);
		try {
			return field.get(instance);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return field;
	}

	public static Object getNonDeclaredFieldValueWithClass(Class<?> clazz, String fieldName) {
		Field field = null;
		try {
			field = clazz.getField(fieldName);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
		field.setAccessible(true);
		try {
			return field.get(clazz);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return field;
	}

	@SuppressWarnings("unchecked")
	public static <T> T getFieldValue(Field field, Object obj) {
		try {
			return (T) field.get(obj);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Field getField(Class<?> clazz, String fieldName) {
		Field field = null;
		try {
			field = clazz.getDeclaredField(fieldName);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
		field.setAccessible(true);
		return field;
	}

	public static void setValue(Object instance, String field, Object value) {
		try {
			Field f = instance.getClass().getDeclaredField(field);
			f.setAccessible(true);
			f.set(instance, value);
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	public static void sendAllPacket(Object packet) {
		for (Player p : Bukkit.getOnlinePlayers()) {
			Object nmsPlayer = getNmsPlayer(p);
			Object connection = null;
			try {
				connection = nmsPlayer.getClass().getField("playerConnection").get(nmsPlayer);
			} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			}
			try {
				connection.getClass().getMethod("sendPacket", Reflection.getClass("{nms}.Packet")).invoke(connection,
						packet);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
		}
	}

	public static void sendListPacket(List<String> players, Object packet) {
		try {
			for (String name : players) {
				Object nmsPlayer = getNmsPlayer(Bukkit.getPlayer(name));
				Object connection = nmsPlayer.getClass().getField("playerConnection").get(nmsPlayer);
				connection.getClass().getMethod("sendPacket", Reflection.getClass("{nms}.Packet")).invoke(connection,
						packet);
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	public static void sendPlayerPacket(Player p, Object packet) {
		Object nmsPlayer = getNmsPlayer(p);
		Object connection = null;
		try {
			connection = nmsPlayer.getClass().getField("playerConnection").get(nmsPlayer);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e1) {
			e1.printStackTrace();
		}
		try {
			connection.getClass().getMethod("sendPacket", Reflection.getClass("{nms}.Packet")).invoke(connection,
					packet);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			e.printStackTrace();
		}
	}

	public static Object getBlockPositionFromLocation(Location loc) {
		Object object = null;
		Class<?> nmsClass = Reflection.getClass("{nms}.BlockPosition");
		for (Constructor<?> nmsConstruct : nmsClass.getDeclaredConstructors()) {
			try {
				object = nmsConstruct.newInstance(loc.getX(), loc.getY(), loc.getZ());
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
			}
		}
		return object;
	}

	public static String getStringFromIChatBaseComponent(Object o) {
		String object = null;
		try {
			Object lobject = ReflectionUtils.invokeMethod(o, "getText");
			object = (String) ReflectionUtils.invokeMethod(lobject, "c");
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException e) {
		}
		return object;
	}

	public static String[] getStringFromIChatBaseComponent(Object[] o) {
		ArrayList<String> texts = new ArrayList<>();
		for (Object str : o) {
			texts.add(getStringFromIChatBaseComponent(((String) str)));
		}
		return texts.toArray(new String[] {});
	}

	public static Location getLocationFromBlockPosition(Object bpos, World w) {
		Location object = null;
		try {
			object = new Location(w, (int) ReflectionUtils.invokeMethod(bpos, "getX"),
					(int) ReflectionUtils.invokeMethod(bpos, "getY"), (int) ReflectionUtils.invokeMethod(bpos, "getZ"));
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException e) {
			e.printStackTrace();
		}
		return object;
	}

	public static Object getIChatBaseComponentFromString(String o) {
		Object object = null;
		try {
			object = ReflectionUtils.invokeMethod(getClass("{nms}.IChatBaseComponent$ChatSerializer"),
					getClass("{nms}.IChatBaseComponent$ChatSerializer"), "a", o);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException e) {
		}
		return object;
	}

	public static Object[] getIChatBaseComponentFromString(String[] o) {
		ArrayList<Object> texts = new ArrayList<>();
		for (String str : o) {
			texts.add(getIChatBaseComponentFromString(str));
		}
		return texts.toArray();
	}

	public static Object getPacket(String packetName, Object... objects) {
		Object object = null;
		Class<?> nmsClass = Reflection.getClass("{nms}." + packetName);
		for (Constructor<?> nmsConstruct : nmsClass.getDeclaredConstructors()) {
			try {
				object = nmsConstruct.newInstance(objects);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
			}
		}
		return object;
	}

}