package fr.adamaq01.evilpingouin;

import java.util.List;
import java.util.function.Consumer;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Lists;

import ch.njol.skript.Skript;
import ch.njol.skript.SkriptAddon;
import fr.adamaq01.evilpingouin.objects.Manager;
import fr.adamaq01.evilpingouin.utils.EffectsManager;
import fr.adamaq01.evilpingouin.utils.EventsManager;
import fr.adamaq01.evilpingouin.utils.ExpressionsManager;
import fr.adamaq01.evilpingouin.utils.PacketInjector;
import fr.adamaq01.evilpingouin.utils.PluginFile;
import fr.adamaq01.evilpingouin.utils.Reflection;
import redis.clients.jedis.Jedis;

public class EvilPingouinSK extends JavaPlugin {

	private static EvilPingouinSK instance;
	private static SkriptAddon addon;
	private Jedis jedis;
	private List<Manager> managers = Lists.newArrayList();
	private PluginFile config;
	private PacketInjector injector;

	@Override
	public void onEnable() {
		getLogger().info("Server version is: " + Reflection.getNmsVersion());
		instance = this;
		injector = new PacketInjector();
		getServer().getPluginManager().registerEvents(new JoinAndQuitListener(), this);
		config = new PluginFile(this, "config.yml");
		if (!config.contains("redis.host")) {
			config.set("redis.host", "localhost");
			config.set("redis.port", "6380");
		}
		config.save();
		jedis = new Jedis(config.getString("redis.host"), config.getInt("redis.port"));
		jedis.connect();
		addon = Skript.registerAddon(this);
		managers.add(new EventsManager(this));
		managers.add(new ExpressionsManager(this));
		managers.add(new EffectsManager(this));
		managers.forEach(new Consumer<Manager>() {
			@Override
			public void accept(Manager m) {
				m.register();
			}
		});
		for (Player pls : getServer().getOnlinePlayers()) {
			injector.addPlayer(pls);
		}
	}

	@Override
	public void onDisable() {
		for (Player pls : getServer().getOnlinePlayers()) {
			injector.removePlayer(pls);
		}
	}

	public static EvilPingouinSK getInstance() {
		return instance;
	}

	public static SkriptAddon getAddon() {
		return addon;
	}

	public PluginFile getConfig() {
		return config;
	}

	public PacketInjector getInjector() {
		return injector;
	}

	public Jedis getJedis() {
		return jedis;
	}

}
