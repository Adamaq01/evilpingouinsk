package fr.adamaq01.evilpingouin.customevents;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class PacketEvent extends PlayerEvent implements Cancellable {

	private String packetName;
	private Object packet;
	private boolean cancel = false;
	
	public PacketEvent(Player who, Object packet, String packetName) {
		super(who);
		this.packet = packet;
		this.packetName = packetName;
	}
	
	public String getPacketName() {
		return packetName;
	}
	
	public Object getPacket() {
		return packet;
	}
	
	private static HandlerList handlers = new HandlerList();
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	@Override
	public boolean isCancelled() {
		return cancel;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancel = cancel;
	}

}
