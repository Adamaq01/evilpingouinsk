package fr.adamaq01.evilpingouin.effects;

import org.bukkit.event.Event;
import org.eclipse.jdt.annotation.Nullable;

import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.util.Kleenean;
import fr.adamaq01.evilpingouin.EvilPingouinSK;

public class EffRedisSet extends Effect {

	private Expression<String> key;
	private Expression<String> value;

	@SuppressWarnings("unchecked")
	@Override
	public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, ParseResult parseResult) {
		this.key = (Expression<String>) exprs[0];
		this.value = (Expression<String>) exprs[1];
		return true;
	}

	@Override
	public String toString(@Nullable Event e, boolean debug) {
		return getClass().getName();
	}

	@Override
	protected void execute(Event e) {
		EvilPingouinSK.getInstance().getJedis().set(key.getSingle(e), value.getSingle(e));
	}

}
